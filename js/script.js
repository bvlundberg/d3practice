// Object
/*
d3.selectAll('.item:nth-child(3)')
	.classed({
		'highlight': true,
		'item': false,
		'bigger': true
	})
*/
//Change style
/*
d3.selectAll('.item:nth-child(3)')
	.style({
		'background': '#268BD2',
		'padding': '10px',
		'margin': '5px',
		'color': '#EEE8D5'
	})
*/
// Data function
/*
var myStyles = ['#268BD2', '#BD3613','#268BD2', '#BD3613','#268BD2', '#BD3613'];

d3.selectAll('.item')
	.data(myStyles)
	.style('background', function(d){
		return d
	})
*/
// Binding data to DOM element
/*
var myStyles = [
	{
		width: 200,
		color: '#268BD2'
	},
	{
		width: 200,
		color: '#BD3613'
	},
	{
		width: 200,
		color: '#268BD2'
	},
	{
		width: 200,
		color: '#BD3613'
	},
	{
		width: 200,
		color: '#268BD2'
	},
	{
		width: 200,
		color: '#BD3613'
	}];

d3.selectAll('.item')
	.data(myStyles)
	.style({
		'color': 'white',
		'background': function(d){
			return d.color;
		},
		width: function(d){
			return d.width + 'px';
		}

	})
*/
// More Data
/*
var myStyles = [
	{
		width: 175,
		name: 'Brandon Lundberg',
		color: '#268BD2'
	},
	{
		width: 140,
		name: 'Justin Watt',
		color: '#BD3613'
	},
	{
		width: 150,
		name: 'Joseph Raco',
		color: '#268BD2'
	},
	{
		width: 160,
		name: 'Alex Plummer',
		color: '#BD3613'
	},
	{
		width: 165,
		name: 'Kelly Madruga',
		color: '#268BD2'
	},
	{
		width: 100,
		name: 'Riley',
		color: '#BD3613'
	}];

d3.selectAll('#chart').selectAll('div')
	.data(myStyles)
	.enter().append('div')
	.classed('item', true)
	.text(function(d){
		return d.name;
	})
	.style({
		'color': 'white',
		'background': function(d){
			return d.color;
		},
		width: function(d){
			return d.width + 'px';
		}

	})
*/
// Creating SVG programmatically
/*
d3.select('#chart')
	.append('svg')
		.attr('width', 600)
		.attr('height', 400)
		.style('background', "#93A1A1")
	.append("rect")
		.attr('x',200)
		.attr('y',100)
		.attr('height',200)
		.attr('width',200)
		.style('fill', '#CB4B19')
	d3.select('svg')
		.append("circle")
			.attr('cx', 300)
			.attr('cy', 200)
			.attr('r', 50)
			.style('fill', '#840043')
*/
// Quantitative scales
/*
var bardata = [20, 30, 105, 15];
var height = 400,
	width = 600,
	barWidth = 50,
	barOffset = 5;
var yScale = d3.scale.linear()
		.domain([0, d3.max(bardata)])
		.range([0,height])
d3.select('#chart').append('svg')
	.attr('width', width)
	.attr('height', height)
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', '#C61C6F')
		.attr('width', barWidth)
		.attr('height', function(d){
			return yScale(d);
		})
		.attr('x', function(d,i){
			return i * (barWidth + barOffset);
		})
		.attr('y', function(d){
			return height - yScale(d);
		})
*/
// Ordinal Scales and color based on data heights
/*
var bardata = [20, 30, 105, 15, 20, 30, 105, 15,20, 30, 105, 15, 20, 30, 105, 15];
var height = 400,
	width = 600,
	barWidth = 50,
	barOffset = 5;

var colors = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range(['#FFB832', '#C61C6F'])

var yScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([0,height])

var xScale = d3.scale.ordinal()
	.domain(d3.range(0,bardata.length))
	.rangeBands([0,width])

d3.select('#chart').append('svg')
	.attr('width', width)
	.attr('height', height)
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', colors)
		.attr('width', xScale.rangeBand())
		.attr('height', function(d){
			return yScale(d);
		})
		.attr('x', function(d,i){
			return xScale(i);
		})
		.attr('y', function(d){
			return height - yScale(d);
		})
*/
// Ordinal Scales and color based on x axis
/*
var bardata = [20, 30, 105, 15, 20, 30, 105, 15,20, 30, 105, 15, 20, 30, 105, 15];
var height = 400,
	width = 600,
	barWidth = 50,
	barOffset = 5;

var colors = d3.scale.linear()
	.domain([0, bardata.length])
	.range(['#FFB832', '#C61C6F'])

var yScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([0,height])

var xScale = d3.scale.ordinal()
	.domain(d3.range(0,bardata.length))
	.rangeBands([0,width])

d3.select('#chart').append('svg')
	.attr('width', width)
	.attr('height', height)
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', function(d,i){
			return colors(i);
		})
		.attr('width', xScale.rangeBand())
		.attr('height', function(d){
			return yScale(d);
		})
		.attr('x', function(d,i){
			return xScale(i);
		})
		.attr('y', function(d){
			return height - yScale(d);
		})
*/
// Opacity on hover!
/*
var bardata = [20, 30, 105, 15, 20, 30, 105, 15,20, 30, 105, 15, 20, 30, 105, 15];
var height = 400,
	width = 600,
	barWidth = 50,
	barOffset = 5;

var colors = d3.scale.linear()
	.domain([0, bardata.length])
	.range(['#FFB832', '#C61C6F'])

var yScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([0,height])

var xScale = d3.scale.ordinal()
	.domain(d3.range(0,bardata.length))
	.rangeBands([0,width])

d3.select('#chart').append('svg')
	.attr('width', width)
	.attr('height', height)
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', function(d,i){
			return colors(i);
		})
		.attr('width', xScale.rangeBand())
		.attr('height', function(d){
			return yScale(d);
		})
		.attr('x', function(d,i){
			return xScale(i);
		})
		.attr('y', function(d){
			return height - yScale(d);
		})
	.on('mouseover', function(d){
		d3.select(this)
			.style('opacity', .5)
	})
	.on('mouseout', function(d){
		d3.select(this)
			.style('opacity', 1)
	})
*/
// Animation
/*
var bardata = [20, 30, 105, 15, 20, 30, 105, 15,20, 30, 105, 15, 20, 30, 105, 15];
var height = 400,
	width = 600,
	barWidth = 50,
	barOffset = 5;

var colors = d3.scale.linear()
	.domain([0, bardata.length])
	.range(['#FFB832', '#C61C6F'])

var yScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([0,height])

var xScale = d3.scale.ordinal()
	.domain(d3.range(0,bardata.length))
	.rangeBands([0,width])

var myChart = d3.select('#chart').append('svg')
	.attr('width', width)
	.attr('height', height)
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', function(d,i){
			return colors(i);
		})
		.attr('width', xScale.rangeBand())

		.attr('x', function(d,i){
			return xScale(i);
		})
		.attr('height', 0)
		.attr('y', height)

	.on('mouseover', function(d){
		d3.select(this)
			.style('opacity', .5)
	})
	.on('mouseout', function(d){
		d3.select(this)
			.style('opacity', 1)
	})

myChart.transition()
	.attr('height', function(d){
		return yScale(d);
	})
	.attr('y', function(d){
		return height - yScale(d);
	})
	.delay(function(d, i){
		return i * 50;
	})
	.duration(2000)
	.ease('elastic')
*/
// Display value in tooltip
/*
var bardata = [20, 30, 105, 15, 20, 30, 105, 15,20, 30, 105, 15, 20, 30, 105, 15];
var height = 400,
	width = 600,
	barWidth = 50,
	barOffset = 5;

var colors = d3.scale.linear()
	.domain([0, bardata.length])
	.range(['#FFB832', '#C61C6F'])

var yScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([0,height])

var xScale = d3.scale.ordinal()
	.domain(d3.range(0,bardata.length))
	.rangeBands([0,width])

var tooltip = d3.select('body').append('div')
	.style('position', 'absolute')
	.style('padding', '0 10px')
	.style('background', 'white')
	.style('opacity', 0)
var myChart = d3.select('#chart').append('svg')
	.attr('width', width)
	.attr('height', height)
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', function(d,i){
			return colors(i);
		})
		.attr('width', xScale.rangeBand())

		.attr('x', function(d,i){
			return xScale(i);
		})
		.attr('height', 0)
		.attr('y', height)

	.on('mouseover', function(d){
		tooltip.transition()
			.style('opacity', .9)

		tooltip.html(d)
			.style('left', (d3.event.pageX - 35) + 'px')
			.style('top', (d3.event.pageY) + 'px')
		d3.select(this)
			.style('opacity', .5)
	})
	.on('mouseout', function(d){
		d3.select(this)
			.style('opacity', 1)
	})

myChart.transition()
	.attr('height', function(d){
		return yScale(d);
	})
	.attr('y', function(d){
		return height - yScale(d);
	})
	.delay(function(d, i){
		return i * 50;
	})
	.duration(2000)
	.ease('elastic')
*/
// Axis
/*
var bardata = [20, 30, 105, 15, 20, 30, 105, 15,20, 30, 105, 15, 20, 30, 105, 15];
var height = 400,
	width = 600,
	barWidth = 50,
	barOffset = 5;

bardata.sort(function compareNumbers(a,b){
	return a - b;
});
var colors = d3.scale.linear()
	.domain([0, bardata.length])
	.range(['#FFB832', '#C61C6F'])

var yScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([0,height])

var xScale = d3.scale.ordinal()
	.domain(d3.range(0,bardata.length))
	.rangeBands([0,width])

var tooltip = d3.select('body').append('div')
	.style('position', 'absolute')
	.style('padding', '0 10px')
	.style('background', 'white')
	.style('opacity', 0)

var myChart = d3.select('#chart').append('svg')
	.attr('width', width)
	.attr('height', height)
	.append('g')
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', function(d,i){
			return colors(i);
		})
		.attr('width', xScale.rangeBand())

		.attr('x', function(d,i){
			return xScale(i);
		})
		.attr('height', 0)
		.attr('y', height)

	.on('mouseover', function(d){
		tooltip.transition()
			.style('opacity', .9)

		tooltip.html(d)
			.style('left', (d3.event.pageX - 35) + 'px')
			.style('top', (d3.event.pageY) + 'px')
		d3.select(this)
			.style('opacity', .5)
	})
	.on('mouseout', function(d){
		d3.select(this)
			.style('opacity', 1)
	})

myChart.transition()
	.attr('height', function(d){
		return yScale(d);
	})
	.attr('y', function(d){
		return height - yScale(d);
	})
	.delay(function(d, i){
		return i * 50;
	})
	.duration(2000)
	.ease('elastic')

var vGuideScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([height, 0])

var vAxis = d3.svg.axis()
	.scale(vGuideScale)
	.orient('left')
	.ticks(10)

var vGuide = d3.select('svg').append('g')
	vAxis(vGuide)

	vGuide.attr('transform', 'translate(35,10)')
	vGuide.selectAll('path')
		.style({fill: 'none', stroke: '#000'})
	vGuide.selectAll('line')
		.style({stroke: '#000'})

var hAxis = d3.svg.axis()
	.scale(xScale)
	.orient('bottom')
	.tickValues(xScale.domain().filter(function(d,i){
		return !(i % (bardata.length/5));
	}))

var hGuide = d3.select('svg').append('g')
	hAxis(hGuide)
	hGuide.attr('transform', 'translate(0, ' + (height - 30) + ')')
	hGuide.selectAll('path')
		.style({fill: 'none', stroke: '#000'})
	hGuide.selectAll('line')
		.style({stroke: '#000'})
*/
// Margins
/*
var bardata = [20, 30, 105, 15, 20, 30, 105, 15,20, 30, 105, 15, 20, 30, 105, 15];
var margin = { top: 30, right: 30, bottom: 40 , left: 50}

var height = 400 - margin.top - margin.bottom,
	width = 600 - margin.left - margin.right,
	barWidth = 50,
	barOffset = 5;

bardata.sort(function compareNumbers(a,b){
	return a - b;
});
var colors = d3.scale.linear()
	.domain([0, bardata.length])
	.range(['#FFB832', '#C61C6F'])

var yScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([0,height])

var xScale = d3.scale.ordinal()
	.domain(d3.range(0,bardata.length))
	.rangeBands([0,width]) // Can put , then decimal for a space between bars

var tooltip = d3.select('body').append('div')
	.style('position', 'absolute')
	.style('padding', '0 10px')
	.style('background', 'white')
	.style('opacity', 0)

var myChart = d3.select('#chart').append('svg')
	.style('background', '#E7E0CB')
	.attr('width', width + margin.left + margin.right)
	.attr('height', height + margin.top + margin.bottom)
	.append('g')
	.attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')')
	.style('background', '#C9D7D6')
	.selectAll('rect').data(bardata)
	.enter().append('rect')
		.style('fill', function(d,i){
			return colors(i);
		})
		.attr('width', xScale.rangeBand())

		.attr('x', function(d,i){
			return xScale(i);
		})
		.attr('height', 0)
		.attr('y', height)

	.on('mouseover', function(d){
		tooltip.transition()
			.style('opacity', .9)

		tooltip.html(d)
			.style('left', (d3.event.pageX - 35) + 'px')
			.style('top', (d3.event.pageY) + 'px')
		d3.select(this)
			.style('opacity', .5)
	})
	.on('mouseout', function(d){
		d3.select(this)
			.style('opacity', 1)
	})

myChart.transition()
	.attr('height', function(d){
		return yScale(d);
	})
	.attr('y', function(d){
		return height - yScale(d);
	})
	.delay(function(d, i){
		return i * 50;
	})
	.duration(2000)
	.ease('elastic')

var vGuideScale = d3.scale.linear()
	.domain([0, d3.max(bardata)])
	.range([height, 0])

var vAxis = d3.svg.axis()
	.scale(vGuideScale)
	.orient('left')
	.ticks(10)

var vGuide = d3.select('svg').append('g')
	vAxis(vGuide)

	vGuide.attr('transform', 'translate(' + margin.left +', ' + margin.top + ')')
	vGuide.selectAll('path')
		.style({fill: 'none', stroke: '#000'})
	vGuide.selectAll('line')
		.style({stroke: '#000'})

var hAxis = d3.svg.axis()
	.scale(xScale)
	.orient('bottom')
	.tickValues(xScale.domain().filter(function(d,i){
		return !(i % (bardata.length/5));
	}))

var hGuide = d3.select('svg').append('g')
	hAxis(hGuide)
	hGuide.attr('transform', 'translate(' + margin.left +', ' + (height + margin.top) + ')')
	hGuide.selectAll('path')
		.style({fill: 'none', stroke: '#000'})
	hGuide.selectAll('line')
		.style({stroke: '#000'})
*/
// External data loading
var bardata = [];

d3.tsv('data.tsv', function(data){
	console.log(data);
	for (key in data){
		bardata.push(data[key].value)
	}

	var margin = { top: 30, right: 30, bottom: 40 , left: 50}

	var height = 400 - margin.top - margin.bottom,
		width = 600 - margin.left - margin.right,
		barWidth = 50,
		barOffset = 5;

	var colors = d3.scale.linear()
		.domain([0, bardata.length])
		.range(['#FFB832', '#C61C6F'])

	var yScale = d3.scale.linear()
		.domain([0, d3.max(bardata)])
		.range([0,height])

	var xScale = d3.scale.ordinal()
		.domain(d3.range(0,bardata.length))
		.rangeBands([0,width]) // Can put , then decimal for a space between bars

	var tooltip = d3.select('body').append('div')
		.style('position', 'absolute')
		.style('padding', '0 10px')
		.style('background', 'white')
		.style('opacity', 0)

	var myChart = d3.select('#chart').append('svg')
		.style('background', '#E7E0CB')
		.attr('width', width + margin.left + margin.right)
		.attr('height', height + margin.top + margin.bottom)
		.append('g')
		.attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')')
		.style('background', '#C9D7D6')
		.selectAll('rect').data(bardata)
		.enter().append('rect')
			.style('fill', function(d,i){
				return colors(i);
			})
			.attr('width', xScale.rangeBand())

			.attr('x', function(d,i){
				return xScale(i);
			})
			.attr('height', 0)
			.attr('y', height)

		.on('mouseover', function(d){
			tooltip.transition()
				.style('opacity', .9)

			tooltip.html(d)
				.style('left', (d3.event.pageX - 35) + 'px')
				.style('top', (d3.event.pageY) + 'px')
			d3.select(this)
				.style('opacity', .5)
		})
		.on('mouseout', function(d){
			d3.select(this)
				.style('opacity', 1)
		})

	myChart.transition()
		.attr('height', function(d){
			return yScale(d);
		})
		.attr('y', function(d){
			return height - yScale(d);
		})
		.delay(function(d, i){
			return i * 50;
		})
		.duration(2000)
		.ease('elastic')

	var vGuideScale = d3.scale.linear()
		.domain([0, d3.max(bardata)])
		.range([height, 0])

	var vAxis = d3.svg.axis()
		.scale(vGuideScale)
		.orient('left')
		.ticks(10)

	var vGuide = d3.select('svg').append('g')
		vAxis(vGuide)

		vGuide.attr('transform', 'translate(' + margin.left +', ' + margin.top + ')')
		vGuide.selectAll('path')
			.style({fill: 'none', stroke: '#000'})
		vGuide.selectAll('line')
			.style({stroke: '#000'})

	var hAxis = d3.svg.axis()
		.scale(xScale)
		.orient('bottom')
		.tickValues(xScale.domain().filter(function(d,i){
			return !(i % (bardata.length/5));
		}))

	var hGuide = d3.select('svg').append('g')
		hAxis(hGuide)
		hGuide.attr('transform', 'translate(' + margin.left +', ' + (height + margin.top) + ')')
		hGuide.selectAll('path')
			.style({fill: 'none', stroke: '#000'})
		hGuide.selectAll('line')
			.style({stroke: '#000'})
});
